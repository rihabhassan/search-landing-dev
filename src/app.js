import React from 'react';
import ReactDOM from 'react-dom';
//import './index.css';
import App from './components/app/App';

// Include CSS dependencies
import 'bootstrap/dist/css/bootstrap.css';
import './styles/lib/bootsnav.css';
import './styles/lib/alien.css';
import 'font-awesome/css/font-awesome.css';
import './styles/styles.css';
ReactDOM.render(<App />, document.getElementById('app'));