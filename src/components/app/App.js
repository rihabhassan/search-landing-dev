import React, { Component } from 'react';
import { HashRouter, Link, Route, Switch } from 'react-router-dom';
import Header from '../header/Header';
import Footer from '../footer/Footer';
import Landing from '../landing/Landing';
import InDev from '../in-dev/InDev';
import SearchModal from '../search-modal/SearchModal';
import './App.css';


class App extends Component {
  render() {
    const displayNone = { display: "none" };   
    return (
      <div id="top" className="home bgfinal screenWidth">
        <div id="sb-site">
          <Header></Header>
          <HashRouter>
            <Switch>
              <Route exact path="/" component={Landing}/>
              <Route exact path="/in-dev" component={InDev}/>
            </Switch>
            </HashRouter>
          <Footer></Footer>
        </div>               
        <SearchModal></SearchModal>
      </div>
    )
  }
}

export default App;
