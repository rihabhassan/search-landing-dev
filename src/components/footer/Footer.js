import React, { Component } from 'react';

class Footer extends Component {
  render() {
    const whiteColor = { color: "white"};
    const marginTopTen = { marginTop: "10px" };
    const marginTopTwentyFive = { marginTop: "25px" };
    const pStyle = { color: "white", fontWeight: "600 !important" };

    return (
      <footer id="footer" className="bg-darker">            
        <div className="container text-sm">
          <div className="row" style={marginTopTen}>
              <div className="col-md-3 u-xs-MarginBottom30">
                <div className="logo u-MarginBottom25">                            
                </div>
                <p style={pStyle}>Allmightytrader is the first peer-to-peer
                    global business, trade, marketing and search platform, based on the sharing economy.</p>
                <h5 className="u-Weight700">Global</h5>
                <p style={pStyle}>
                    Email: info@allmightytrader.com
                </p>
              </div>
              <div className="col-md-3 u-xs-MarginBottom30">
                  <ul className="light-gray-link border-bottom-link list-unstyled u-LineHeight1 u-PaddingRight40 u-xs-PaddingRight0">
                      <li> <a className="redcolor" href="#/in-dev" style={pStyle}><i
                                  className="fa fa-angle-right u-MarginRight10" aria-hidden="true"></i>About Us</a></li>
                      <li> <a href="#/in-dev" style={pStyle}><i className="fa fa-angle-right u-MarginRight10"
                                  aria-hidden="true"></i>Services</a></li>
                      <li> <a href="#/in-dev" style={pStyle}><i className="fa fa-angle-right u-MarginRight10"
                                  aria-hidden="true"></i>Benefits</a></li>
                      <li> <a href="#/in-dev" style={pStyle}><i className="fa fa-angle-right u-MarginRight10"
                                  aria-hidden="true"></i>Deals</a></li>
                      <li> <a href="#/in-dev" style={pStyle}><i className="fa fa-angle-right u-MarginRight10 u-"
                                  aria-hidden="true"></i>Reseller</a></li>
                      <li> <a href="#/in-dev" style={pStyle}><i className="fa fa-angle-right u-MarginRight10"
                                  aria-hidden="true"></i>Affiliate</a></li>

                      <li> <a href="#/in-dev" style={pStyle}><i className="fa fa-angle-right u-MarginRight10"
                                  aria-hidden="true"></i>Partner With Us</a></li>
                      <li> <a href="#/in-dev" style={pStyle}><i
                                  className="fa fa-angle-right u-MarginRight10" aria-hidden="true"></i>Investor
                              Relations</a></li>
                  </ul>
              </div>
              <div className="col-md-3 u-xs-MarginBottom30" style={marginTopTwentyFive}>
                  <ul className="light-gray-link border-bottom-link list-unstyled u-LineHeight1 u-PaddingRight40 u-xs-PaddingRight0">
                      <li> <a href="#/in-dev" style={pStyle}><i className="fa fa-angle-right u-MarginRight10"
                                  aria-hidden="true"></i>FAQ</a></li>
                      <li> <a href="#/in-dev" style={pStyle}><i className="fa fa-angle-right u-MarginRight10"
                                  aria-hidden="true"></i>Blog</a></li>

                      <li> <a href="#/in-dev" style={pStyle}><i className="fa fa-angle-right u-MarginRight10"
                                  aria-hidden="true"></i>Feedback</a></li>
                      <li> <a href="#/in-dev" style={pStyle}><i className="fa fa-angle-right u-MarginRight10"
                                  aria-hidden="true"></i>Terms & Condition</a></li>
                      <li> <a href="#/in-dev" style={pStyle}><i className="fa fa-angle-right u-MarginRight10"
                                  aria-hidden="true"></i>Privacy Policy</a></li>
                      <li> <a href="#/in-dev" style={pStyle}><i className="fa fa-angle-right u-MarginRight10"
                                  aria-hidden="true"></i>Contact Us</a></li>
                  </ul>
              </div>
              <div className="col-md-3 u-xs-MarginBottom30">
                  <h5 className="deal text-uppercase u-Weight800 u-LetterSpacing2"><a href="#/in-dev" style={pStyle}><i
                              className="fa fa-angle-right u-MarginRight10" aria-hidden="true"></i>Deal Competetion</a></h5>
                  <ul className="light-gray-link border-bottom-link list-unstyled u-LineHeight1 u-PaddingRight40 u-xs-PaddingRight0">

                      <li> <a href="#/in-dev" style={pStyle}><i className="fa fa-angle-right u-MarginRight10"
                                  aria-hidden="true"></i>Allmighty Blogger</a></li>
                  </ul>
              </div>
              <div className="col-md-3">
                  <h5 className="text-uppercase u-Weight800 u-LetterSpacing2 u-MarginTop0">Follow Us</h5>
                  <div className="social-links sl-default gray-border-links border-link circle-link colored-hover">
                      <a href="#/in-dev" className="facebook">
                          <i className="fa fa-facebook"></i>
                      </a>
                      <a href="#/in-dev" className="twitter">
                          <i className="fa fa-twitter"></i>
                      </a>
                      <a href="#/in-dev" className="g-plus">
                          <i className="fa fa-google-plus"></i>
                      </a>
                      <a href="#/in-dev" className="youtube">
                          <i className="fa fa-youtube"></i>
                      </a>
                  </div>
              </div>
          </div>
          <div className="text-center text-sm u-MarginTop30" style={whiteColor}>
            <div className="footer-separator"></div>
            <p className="u-MarginBottom0 u-PaddingTop30 u-PaddingBottom30">
                Algeria, Argentina, Armenia,
                Australia,
                Austria,
                Azerbaijan,
                Bangladesh,
                Belarus,
                Belgium,
                Brazil,
                Bulgaria,
                Canada,
                China,
                Colombia,
                Croatia,
                Czech Republic,
                Denmark,
                Egypt,
                Finland,
                France,
                Georgia,
                Germany,
                Greece,
                Hong Kong,
                Hungary,
                India,
                Iran,
                Ireland,
                Italy,
                Japan,
                Kazakhstan,
                Kyrgyzstan,
                Latvia,
                Lebanon,
                Luxembourg,
                Mexico,
                Moldova,
                Monaco,
                Morocco,
                Netherlands,
                New Zealand,
                Norway,
                Peru,
                Poland,
                Portugal,
                Romania,
                Russian Federation,
                San Marino,
                Serbia,
                Singapore,
                Slovakia,
                Slovenia,
                South Africa,
                South Korea,
                Spain,
                Sri Lanka,
                Sweden,
                Switzerland,
                Taiwan,
                Thailand,
                Tunisia,
                Ukraine,
                United Arab Emirates,
                United Kingdom,
                United States,
                Vietnam
            </p>
          </div>            
        </div>
      </footer>
    )
  }
}

export default Footer;
