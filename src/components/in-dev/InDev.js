import React, { Component } from 'react';
import './InDev.css';
import 'jquery/src/jquery';
import 'bootstrap/dist/js/bootstrap';

class InDev extends Component {
  constructor(props) {
    super(props);
    this.searchClick = this.searchHandler.bind(this);
  }
  searchHandler() {
    const $ = jQuery;
    var searchText = $('#keyword1').val();
    var modalEl = $('#searchResultModal');
    modalEl.find('.search-keywords').text(searchText);
    modalEl.modal();

    return false;
}

  render() {
    return (
      <div id="content-wrapper" className="widescreen">
        <div className="container">
          <div className="alert alert-warning">Non-landing pages are unavailable.</div>
          <p>This is R&D into a React implementation of the site landing page.</p>
        </div>
     </div>
      );
  }
}

export default InDev;