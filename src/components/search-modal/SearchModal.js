import React, { Component } from 'react';

class SearchModal extends Component {
  render() {
    const displayNone = { display: "none" };
    return (
      <div className="modal fade" id="searchResultModal" tabIndex="-1" role="dialog" aria-labelledby="searchResultModalLabel" style={displayNone}>
          <div className="modal-dialog" role="document">
              <div className="modal-content">
                  <div className="modal-header">
                      <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 className="modal-title" id="searchResultModalLabel">Test Search Result</h4>
                  </div>
                  <div className="modal-body">
                      <h4>Searching for <span className="search-keywords">...</span></h4>
                      Test search result here..
                  </div>
                  <div className="modal-footer">
                      <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
              </div>
          </div>
      </div>
    )
  }
}

export default SearchModal;
