import React, { Component } from 'react';
import './Header.css';

class Header extends Component {
  render() {
    const whiteColor = { color: "white" };
    const navMarginTop = { marginTop: "-12px" };

    return (
      <header>
        <nav id="mainNav" className="navbar navbar-default navbar-fixed bootsnav">
          <div className="top-search">
            <div className="container">
              <div className="input-group">
                <span className="input-group-addon"><i className="fa fa-search"></i></span>
                <input type="text" className="form-control" placeholder="Search"></input>
                <span className="input-group-addon close-search"><i className="fa fa-times"></i></span>
              </div>
            </div>
          </div>
          <div className="container">
            <div className="navbar-header">
              <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                <i className="fa fa-bars"></i>
              </button>
              <a className="navbar-brand" href="#">
                <img src="images/logo-amt.png" className="logo logo-scrolled" alt="Logo" />
              </a>
            </div>
            <div className="collapse navbar-collapse" id="navbar-menu">
              <ul className="nav navbar-nav navbar-right" data-in="" data-out="" style={navMarginTop}>
                <li>
                  <a style={whiteColor} href="#/in-dev" id="loginButton">
                      <i className="fa fa-user"></i> <b>Signin</b>
                  </a>
                </li>
                <li>
                  <a style={whiteColor} href="#/in-dev">
                      <i className="fa fa-pencil-square-o"></i> <b>Join Us</b>
                  </a>
                </li>
                <li>
                  <a style={whiteColor} href="#/in-dev">
                      <i className="fa fa-user"></i> <b>Dashboard</b>
                  </a>
                </li>
                <li><div id="google_translate_element"></div></li>
              </ul>
            </div>
          </div>
        </nav>
        <div className="clearfix"></div>
      </header>
    )
  }
}

export default Header;
