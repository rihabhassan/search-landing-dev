import React, { Component } from 'react';
import './Landing.css';
import 'jquery/src/jquery';
import 'bootstrap/dist/js/bootstrap';

class Landing extends Component {
  constructor(props) {
    super(props);
    this.searchClick = this.searchHandler.bind(this);
  }
  searchHandler() {
    const $ = jQuery;
    var searchText = $('#keyword1').val();
    var modalEl = $('#searchResultModal');
    modalEl.find('.search-keywords').text(searchText);
    modalEl.modal();

    return false;
}

  render() {    
    const navMarginTop = { marginTop: "-12px" };
    const searchErrorStyle = { fontSize: "14px", color: "red",fontWeight: "bold", marginLeft: "200px" };
    const marginTophundred = { marginTop: "-100px" };    
    return (
      <div id="content-wrapper" className="widescreen">
        <section className="ImageBackground js-FullHeight" data-overlay="9">
          <div className="ImageBackground__holder" style={navMarginTop}>
            <img src="images/digital-banner.jpg" alt="..." />
          </div>
          <div className="container">
            <div className="hero-container">
              <div className="row text-white">
                <h1 className="landing-title text-white u-xs-FontSize20">LOOKING FOR SOMETHING?</h1>
              </div>
              <div className="row text-white">
                <h3 className="landing-subtitle text-white u-xs-FontSize20">Enter a world of business opportunities &amp; Endless Rewards</h3>
              </div>
            </div>
            <form action="/search" className="form-inline search-form formClass" method="GET">
              <input type="hidden" disabled name="_csrf" value="8MOXzt1nl_-wFlZ717j8o5XfF3cR30H5Px8xLMpazuWTru2c6Vbij9M7ZyrmyovP7OpPPGa2LqxZZWFWqRu4ug==" />

              <div className="input-group">
                <input type="text" className="form-control searchbarhome" id="keyword1" name="keyword"
                  placeholder="Find what you want. Make an Enquiry" data-toggle="popover" data-trigger="manual"
                  data-placement="bottom" />                                    
                  <input type="hidden" id="keyword-en" disabled name="keyword_en" value="" />                  
                  <span className="input-group-btn">
                    <button onClick={this.searchClick} className="btn btn-primary btn-search btnHome" type="button">
                      <i className="fa fa-lg fa-search"></i>
                    </button>
                  </span>
              </div>
              <div className="search-error-cont" style={searchErrorStyle}></div>
            </form>
          </div>
        </section>
        <section style={marginTophundred}>
          <div className="container">
            <div className="row u-MarginTop20">
              <div className="col-md-4 col-sm-6 text-center u-MarginBottom30">
                <div className="u-PaddingLeft15 u-PaddingRight15">
                  <div className="Thumb Thumb--182px u-Rounded">
                    <a href="#/in-dev">
                      <img className="img-responsive" src="images/service-icon2.png" alt="..." />
                    </a>
                  </div>
                  <h3>
                    <a className="Blurb__hoverText" href="#/in-dev"><b>Services</b></a>
                  </h3>
                </div>
              </div>
              <div className="col-md-4 col-sm-6 text-center u-MarginBottom30">
                <div className="u-PaddingLeft15 u-PaddingRight15">
                  <div className="Thumb Thumb--182px u-Rounded">
                    <a href="#/in-dev">
                        <img className="img-responsive" src="images/deal-icon.png" alt="..." />
                    </a>
                  </div>
                  <h3>
                    <a className="Blurb__hoverText" href="#/in-dev"><b>Deals</b></a>
                  </h3>
                </div>
              </div>
              <div className="col-md-4 col-md-offset-0 col-sm-6 col-sm-offset-3 text-center u-MarginBottom30">
                <div className="u-PaddingLeft15 u-PaddingRight15">
                  <div className="Thumb Thumb--182px u-Rounded">
                      <a href="#/in-dev">
                          <img className="img-responsive" src="images/reseller-icon.png" alt="..."></img>
                      </a>
                  </div>
                  <h3>
                      <a className="Blurb__hoverText" href="#/in-dev"><b>Resellers</b></a>
                  </h3>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    )
  }
}

export default Landing;