import { LanguageTranslator } from './language-translator';

(function($) {
    $(document).ready(function() {
      var translator = new LanguageTranslator();
      // Global function called by Google Translator script
      window.googleTranslateElementInit = translator.createWidget("google_translate_element");

    

      var waitForEl = function (selector, callback, maxTimes = false) {
          var element = $(selector);
          console.log('wait for el:', selector);
          if (element.length && element.is(':visible')) {
          callback(element);
          } else {
          if (maxTimes === false || maxTimes > 0) {
              (maxTimes != false) && maxTimes-- ;
              setTimeout(function () {
              waitForEl(selector, callback, maxTimes);
              }, 100);
          }
          }
      };

      /*==============================================
      Back to top
      ===============================================*/
      $('body').append("<a href='#top' class='BackToTop BackToTop--hide ScrollTo'><i class='fa fa-angle-up'></i></a>");

      var $liftOff = $('.BackToTop');
      $(window).on('scroll', function () {
          if ($(window).scrollTop() > 150) {
              $liftOff.addClass('BackToTop--show').removeClass('BackToTop--hide');
          } else {
              $liftOff.addClass('BackToTop--hide').removeClass('BackToTop--show');
          }
      });

      $('.ScrollTo').on('click', function(e) {
          e.preventDefault();
          var element_id = $(this).attr('href');
          $('html, body').animate({
              scrollTop: $(element_id).offset().top -60
          },500);
      });

      /*==============================================
      Retina support added
      ===============================================*/
      if (window.devicePixelRatio > 1 && $.fn.imagesLoaded) {
          $(".retina, .navbar-brand img, .logo img").imagesLoaded(function () {
              $(".retina, .navbar-brand img, .logo img").each(function () {
                  var src = $(this).attr("src").replace(".", "@2x.");
                  var h = $(this).height();
                  $(this).attr("src", src).css({height: h, width: "auto"});
              });
          });
      }

      $('.ImageBackground').each(function(){            
          var $this = $(this),
              $imgHolder = $this.children('.ImageBackground__holder'),
              thisIMG = $imgHolder.children().attr('src'),
              thisURL = 'url('+thisIMG+')';
              $imgHolder.css('background-image', thisURL);
      });

      var browserLanguage = translator.getBrowserLanguage();
      // Select a language to translate to based on the browser language 
      // if it isn't English
      if (browserLanguage.match(/^en/i) === null) {
          waitForEl('.goog-te-gadget-simple', function(dropdownEl) {
              console.log('BrowserLanguage:', browserLanguage);
              dropdownEl.trigger('click');
              console.log('popup here');
              waitForEl('iframe.goog-te-menu-frame', function(iframeEl){
                  var iframeEl = iframeEl.contents();
                  var optionEl = iframeEl.find('.goog-te-menu2 .goog-te-menu2-item .text').filter(
                      function() {
                          var map = translator.findLanguageMap(browserLanguage);
                          if (map === null) {
                              map = translator.findLanguageMap('en');
                          }
                          return $(this).text().toLowerCase() === map.browserLanguageText.toLowerCase();
                      }
                  );
                  optionEl.trigger('click');
                  
              }, 100);
          }, 100);
      } else {
          
      }
    });
})(jQuery);