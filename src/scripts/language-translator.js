class LanguageTranslator {
  constructor() {
    this.init();
  }

  init() {
    this.currentBLang = navigator.language || navigator.userLanguage;    

    this.languageMap = [
      {
        browserLanguage: 'af',
        browserLanguageText: 'Afrikaans',
        googleLanguage: 'af'
      },

      {
        browserLanguage: 'sq',
        browserLanguageText: 'Albanian',
        googleLanguage: 'sq'
      },

      {
        browserLanguage: 'am',
        browserLanguageText: 'Amharic',
        googleLanguage: 'am'
      },

      {
        browserLanguage: 'ar',
        browserLanguageText: 'Arabic',
        googleLanguage: 'ar'
      },

      {
        browserLanguage: 'hy',
        browserLanguageText: 'Armenian',
        googleLanguage: 'hy'
      },

      {
        browserLanguage: 'az',
        browserLanguageText: 'Azeerbaijani',
        googleLanguage: 'az'
      },

      {
        browserLanguage: 'eu',
        browserLanguageText: 'Basque',
        googleLanguage: 'eu'
      },

      {
        browserLanguage: 'be',
        browserLanguageText: 'Belarusian',
        googleLanguage: 'be'
      },

      {
        browserLanguage: 'bn',
        browserLanguageText: 'Bengali',
        googleLanguage: 'bn'
      },

      {
        browserLanguage: 'bs',
        browserLanguageText: 'Bosnian',
        googleLanguage: 'bs'
      },

      {
        browserLanguage: 'bg',
        browserLanguageText: 'Bulgarian',
        googleLanguage: 'bg'
      },

      {
        browserLanguage: 'ca',
        browserLanguageText: 'Catalan',
        googleLanguage: 'ca'
      },

      {
        browserLanguage: 'ceb',
        browserLanguageText: 'Cebuano',
        googleLanguage: 'ceb'
      },

      {
        browserLanguage: 'zh-CN',
        browserLanguageText: 'Chinese (Simplified)',
        googleLanguage: 'zh-CN'
      },
      {
        browserLanguage: 'zh-TW',
        browserLanguageText: 'Chinese (Traditional)',
        googleLanguage: 'zh-TW'
      },
      {
        browserLanguage: 'co',
        browserLanguageText: 'Corsican',
        googleLanguage: 'co'
      },
      {
        browserLanguage: 'hr',
        browserLanguageText: 'Croatian',
        googleLanguage: 'hr'
      },
      {
        browserLanguage: 'cs',
        browserLanguageText: 'Czech',
        googleLanguage: 'cs'
      },
      {
        browserLanguage: 'da',
        browserLanguageText: 'Danish',
        googleLanguage: 'da'
      },
      {
        browserLanguage: 'nl',
        browserLanguageText: 'Dutch',
        googleLanguage: 'nl'
      },

      {
        browserLanguage: /^en/i,
        browserLanguageText: 'English',
        googleLanguage: 'en'
      },
          
      {
        browserLanguage: 'eo',
        browserLanguageText: 'Esperanto',
        googleLanguage: 'eo'
      },
          
      {
        browserLanguage: 'et',
        browserLanguageText: 'Estonian',
        googleLanguage: 'et'
      },
          
      {
        browserLanguage: 'fi',
        browserLanguageText: 'Finnish',
        googleLanguage: 'fi'
      },
          
      {
        browserLanguage: 'fr',
        browserLanguageText: 'French',
        googleLanguage: 'fr'
      },

      {
        browserLanguage: 'fy',
        browserLanguageText: 'Frisian',
        googleLanguage: 'fy'
      },

      {
        browserLanguage: 'gl',
        browserLanguageText: 'Galician',
        googleLanguage: 'gl'
      },

      {
        browserLanguage: 'ka',
        browserLanguageText: 'Georgian',
        googleLanguage: 'ka'
      },

      {
        browserLanguage: 'de',
        browserLanguageText: 'German',
        googleLanguage: 'de'
      },

      {
        browserLanguage: 'el',
        browserLanguageText: 'Greek',
        googleLanguage: 'el'
      },

      {
        browserLanguage: 'gu',
        browserLanguageText: 'Gujarati',
        googleLanguage: 'gu'
      },

      {
        browserLanguage: 'ht',
        browserLanguageText: 'Haitian Creole',
        googleLanguage: 'ht'
      },

      {
        browserLanguage: 'ha',
        browserLanguageText: 'Hausa',
        googleLanguage: 'ha'
      },

      {
        browserLanguage: 'haw',
        browserLanguageText: 'Hawaiian',
        googleLanguage: 'haw'
      },

      {
        browserLanguage: 'he',
        browserLanguageText: 'Hebrew',
        googleLanguage: 'he'
      },

      {
        browserLanguage: 'hi',
        browserLanguageText: 'Hindi',
        googleLanguage: 'hi'
      },

      {
        browserLanguage: 'hmn',
        browserLanguageText: 'Hmong',
        googleLanguage: 'hmn'
      },

      {
        browserLanguage: 'hu',
        browserLanguageText: 'Hungarian',
        googleLanguage: 'hu'
      },

      {
        browserLanguage: 'is',
        browserLanguageText: 'Icelandic',
        googleLanguage: 'is'
      },

      {
        browserLanguage: 'ig',
        browserLanguageText: 'Igbo',
        googleLanguage: 'ig'
      },

      {
        browserLanguage: 'id',
        browserLanguageText: 'Indonesian',
        googleLanguage: 'id'
      },

      {
        browserLanguage: 'ga',
        browserLanguageText: 'Irish',
        googleLanguage: 'ga'
      },

      {
        browserLanguage: 'it',
        browserLanguageText: 'Italian',
        googleLanguage: 'it'
      },

      {
        browserLanguage: 'ja',
        browserLanguageText: 'Japanese',
        googleLanguage: 'ja'
      },

      {
        browserLanguage: 'jw',
        browserLanguageText: 'Javanese',
        googleLanguage: 'jw'
      },

      {
        browserLanguage: 'kn',
        browserLanguageText: 'Kannada',
        googleLanguage: 'kn'
      },

      {
        browserLanguage: 'kk',
        browserLanguageText: 'Kazakh',
        googleLanguage: 'kk'
      },

      {
        browserLanguage: 'km',
        browserLanguageText: 'Khmer',
        googleLanguage: 'km'
      },

      {
        browserLanguage: 'ko',
        browserLanguageText: 'Korean',
        googleLanguage: 'ko'
      },

      {
        browserLanguage: 'ku',
        browserLanguageText: 'Kurdish',
        googleLanguage: 'ku'
      },

      {
        browserLanguage: 'ky',
        browserLanguageText: 'Kyrgyz',
        googleLanguage: 'ky'
      },

      {
        browserLanguage: 'lo',
        browserLanguageText: 'Lao',
        googleLanguage: 'lo'
      },

      {
        browserLanguage: 'la',
        browserLanguageText: 'Latin',
        googleLanguage: 'la'
      },

      {
        browserLanguage: 'lv',
        browserLanguageText: 'Latvian',
        googleLanguage: 'lv'
      },

      {
        browserLanguage: 'lt',
        browserLanguageText: 'Lithuanian',
        googleLanguage: 'lt'
      },

      {
        browserLanguage: 'lb',
        browserLanguageText: 'Luxembourgish',
        googleLanguage: 'lb'
      },

      {
        browserLanguage: 'mk',
        browserLanguageText: 'Macedonian',
        googleLanguage: 'mk'
      },

      {
        browserLanguage: 'mg',
        browserLanguageText: 'Malagasy',
        googleLanguage: 'mg'
      },

      {
        browserLanguage: 'ms',
        browserLanguageText: 'Malay',
        googleLanguage: 'ms'
      },

      {
        browserLanguage: 'ml',
        browserLanguageText: 'Malayalam',
        googleLanguage: 'ml'
      },

      {
        browserLanguage: 'mt',
        browserLanguageText: 'Maltese',
        googleLanguage: 'mt'
      },

      {
        browserLanguage: 'mi',
        browserLanguageText: 'Maori',
        googleLanguage: 'mi'
      },

      {
        browserLanguage: 'mr',
        browserLanguageText: 'Marathi',
        googleLanguage: 'mr'
      },

      {
        browserLanguage: 'mn',
        browserLanguageText: 'Mongolian',
        googleLanguage: 'mn'
      },

      {
        browserLanguage: 'my',
        browserLanguageText: 'Myanmar (Burmese)',
        googleLanguage: 'my'
      },

      {
        browserLanguage: 'ne',
        browserLanguageText: 'Nepali',
        googleLanguage: 'ne'
      },

      {
        browserLanguage: 'no',
        browserLanguageText: 'Norwegian',
        googleLanguage: 'no'
      },

      {
        browserLanguage: 'ny',
        browserLanguageText: 'Nyanja (Chichewa)',
        googleLanguage: 'ny'
      },

      {
        browserLanguage: 'ps',
        browserLanguageText: 'Pashto',
        googleLanguage: 'ps'
      },

      {
        browserLanguage: 'fa',
        browserLanguageText: 'Persian',
        googleLanguage: 'fa'
      },

      {
        browserLanguage: 'pl',
        browserLanguageText: 'Polish',
        googleLanguage: 'pl'
      },

      {
        browserLanguage: 'pt',
        browserLanguageText: 'Portuguese (Portugal, Brazil)',
        googleLanguage: 'pt'
      },

      {
        browserLanguage: 'pa',
        browserLanguageText: 'Punjabi',
        googleLanguage: 'pa'
      },

      {
        browserLanguage: 'ro',
        browserLanguageText: 'Romanian',
        googleLanguage: 'ro'
      },

      {
        browserLanguage: 'ru',
        browserLanguageText: 'Russian',
        googleLanguage: 'ru'
      },

      {
        browserLanguage: 'sm',
        browserLanguageText: 'Samoan',
        googleLanguage: 'sm'
      },

      {
        browserLanguage: 'gd',
        browserLanguageText: 'Scots Gaelic',
        googleLanguage: 'gd'
      },

      {
        browserLanguage: 'sr',
        browserLanguageText: 'Serbian',
        googleLanguage: 'sr'
      },

      {
        browserLanguage: 'st',
        browserLanguageText: 'Sesotho',
        googleLanguage: 'st'
      },

      {
        browserLanguage: 'sn',
        browserLanguageText: 'Shona',
        googleLanguage: 'sn'
      },

      {
        browserLanguage: 'sd',
        browserLanguageText: 'Sindhi',
        googleLanguage: 'sd'
      },

      {
        browserLanguage: 'si',
        browserLanguageText: 'Sinhala (Sinhalese)',
        googleLanguage: 'si'
      },

      {
        browserLanguage: 'sk',
        browserLanguageText: 'Slovak',
        googleLanguage: 'sk'
      },

      {
        browserLanguage: 'sl',
        browserLanguageText: 'Slovenian',
        googleLanguage: 'sl'
      },

      {
        browserLanguage: 'so',
        browserLanguageText: 'Somali',
        googleLanguage: 'so'
      },

      {
        browserLanguage: 'es',
        browserLanguageText: 'Spanish',
        googleLanguage: 'es'
      },

      {
        browserLanguage: 'su',
        browserLanguageText: 'Sundanese',
        googleLanguage: 'su'
      },

      {
        browserLanguage: 'sw',
        browserLanguageText: 'Swahili',
        googleLanguage: 'sw'
      },

      {
        browserLanguage: 'sv',
        browserLanguageText: 'Swedish',
        googleLanguage: 'sv'
      },

      {
        browserLanguage: 'tl',
        browserLanguageText: 'Tagalog (Filipino)',
        googleLanguage: 'tl'
      },

      {
        browserLanguage: 'tg',
        browserLanguageText: 'Tajik',
        googleLanguage: 'tg'
      },

      {
        browserLanguage: 'ta',
        browserLanguageText: 'Tamil',
        googleLanguage: 'ta'
      },

      {
        browserLanguage: 'te',
        browserLanguageText: 'Telugu',
        googleLanguage: 'te'
      },

      {
        browserLanguage: 'th',
        browserLanguageText: 'Thai',
        googleLanguage: 'th'
      },

      {
        browserLanguage: 'tr',
        browserLanguageText: 'Turkish',
        googleLanguage: 'te'
      },

      {
        browserLanguage: 'uk',
        browserLanguageText: 'Ukrainian',
        googleLanguage: 'uk'
      },

      {
        browserLanguage: 'ur',
        browserLanguageText: 'Urdu',
        googleLanguage: 'ur'
      },

      {
        browserLanguage: 'uz',
        browserLanguageText: 'Uzbek',
        googleLanguage: 'uz'
      },

      {
        browserLanguage: 'vi',
        browserLanguageText: 'Vietnamese',
        googleLanguage: 'vi'
      },

      {
        browserLanguage: 'cy',
        browserLanguageText: 'Welsh',
        googleLanguage: 'cy'
      },

      {
        browserLanguage: 'xh',
        browserLanguageText: 'Xhosa',
        googleLanguage: 'xh'
      },

      {
        browserLanguage: 'yi',
        browserLanguageText: 'Yiddish',
        googleLanguage: 'yi'
      },

      {
        browserLanguage: 'yo',
        browserLanguageText: 'Yoruba',
        googleLanguage: 'yo'
      },

      {
        browserLanguage: 'zu',
        browserLanguageText: 'Zulu',
        googleLanguage: 'zu'
      }
    
    ];

  }

  findLanguageMap(browserLanguage) {
    for (var i = 0; i < this.languageMap.length; i++) {
      var currentBrowserLanguage = this.languageMap[i].browserLanguage;
      if (typeof currentBrowserLanguage === 'string') {
        currentBrowserLanguage = new RegExp('^' + currentBrowserLanguage + '$');
      }
      if (browserLanguage.match(currentBrowserLanguage) !== null) {        
        return this.languageMap[i];
      }
    }
  }

  getBrowserLanguage() {
    return this.currentBLang;
  }

  // NOTE: Include
  // <script src=”//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit”></script>
  createWidget(elementId) {
    var map = this.findLanguageMap(this.currentBLang);
    var googleLanguage = (map === null) ? 'en' : map.googleLanguage;

    return function () {
      return new google.translate.TranslateElement({ pageLanguage: googleLanguage, layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false }, elementId);
    }
  }

  // Create the google translator widget and automatically select the language
  // based on the browser language.
  createWidgetAndSelectLanguage(elementId) {
    new LanguageTranslator();    
    var waitForEl = function (selector, callback, maxTimes = false) {
      var element = $(selector);      
      if (element.length && element.is(':visible')) {
        callback(element);
      } else {
        if (maxTimes === false || maxTimes > 0) {
          (maxTimes != false) && maxTimes--;
          setTimeout(function () {
            waitForEl(selector, callback, maxTimes);
          }, 100);
        }
      }
    };
  }
}

module.exports = {
  LanguageTranslator: LanguageTranslator
};