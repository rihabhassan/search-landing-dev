# AMT Search Landing
Build of AMT site landing in React as part of R&D.

## Usage
In a browser open the static `dist/index.html` to see the site that was build from source.

## Development
One a command-line enter the following:

1. `npm install`

2. `npm run build-dev`

If all goes well, the source code will be build into the dist/ directory.