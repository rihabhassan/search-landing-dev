const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = [{
  entry: { main: './src/app.js' },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'main.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.(ttf|eot|svg|woff|woff2)$/,
        use: {
          loader: "file-loader"
        }
      },
      {
        test: /\.(png|jpe?g|gif|svg|tif|bmp)$/,
        use: {
          loader: "file-loader"
        }
      },
         
      {
        test: /\.css$/,
        use: [ MiniCssExtractPlugin.loader, 'css-loader' ]
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./src/index.html",
      filename: "./index.html"
    }),
    new MiniCssExtractPlugin(),
    new CopyWebpackPlugin([{
      from: './src/images',
      to: './images'
    }])
  ]
 },
 {
     name: "deps",
     // mode: "development || "production",
     //entry: ["./src/scripts/lib/jquery", "./src/scripts/lib/bootstrap", "./src/scripts/lib/bootsnav", "./src/scripts/language-translator", "./src/scripts/app.js" ],
     entry: [ './src/deps.js' ],
     output: {
       path: path.resolve(__dirname, "dist"),
       filename: "deps.js",
       library: "deps_[hash]"
    },
    plugins: [
      new webpack.ProvidePlugin({
        $: "jquery",
        jquery: "jquery",
        "window.jQuery": "jquery",
        jQuery:"jquery"
      })
     ]
   }
 ]; 